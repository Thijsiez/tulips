package com.hackjunction.tulips

import okhttp3.HttpUrl

object ServerInfo {
    private val scheme = "https"
    private val host = "nickverbeet.com"
    private val trackerPath = "tracker"
    private val newTrackerPath = "new"

    val NEW_TRACKER_ENDPOINT: HttpUrl by lazy {
        HttpUrl.Builder()
                .scheme(scheme)
                .host(host)
                .addPathSegment(trackerPath)
                .addPathSegment(newTrackerPath)
                .build()
    }

    fun getTrackingUrlString(trackingId: String) =
        HttpUrl.Builder()
                .scheme(scheme)
                .host(host)
                .addPathSegment(trackerPath)
                .addPathSegment(trackingId)
                .build()
                .toString()

    val GEOFENCE_ENTERED_ENDPOINT: HttpUrl by lazy {
        HttpUrl.Builder()
                .scheme(scheme)
                .host(host)
                .addPathSegment(trackerPath)
                .build()
    }
}