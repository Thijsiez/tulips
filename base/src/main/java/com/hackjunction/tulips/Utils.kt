package com.hackjunction.tulips

fun <T> withIfNotNull(receiver: T?, block: (T.() -> Unit)) = receiver?.block()
fun printerr(message: Any?) = System.err.println(message)