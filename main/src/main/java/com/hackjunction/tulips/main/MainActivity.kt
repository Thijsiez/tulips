package com.hackjunction.tulips.main

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.ResultReceiver
import android.support.v7.app.AppCompatActivity
import com.hackjunction.tulips.main.ShareActivity.Companion.TRACKING_ID
import com.hackjunction.tulips.withIfNotNull
import com.hackjunction.tulips.main.StartTrackingService.Companion.CALLBACK
import com.hackjunction.tulips.main.StartTrackingService.Companion.RESULT_ERROR_MESSAGE
import com.hackjunction.tulips.main.StartTrackingService.Companion.RESULT_ERROR
import com.hackjunction.tulips.main.StartTrackingService.Companion.RESULT_SUCCESS
import com.hackjunction.tulips.main.StartTrackingService.Companion.RESULT_TRACKING_ID
import com.hackjunction.tulips.printerr
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val startTrackingCallback = object : ResultReceiver(Handler()) {
        override fun onReceiveResult(resultCode: Int, resultData: Bundle?) {
            //Hide the progress indicator
            trackingStartProgress.animate().alpha(0f)
            startTracking.isEnabled = true

            //Check if data was passed
            withIfNotNull(resultData) {
                //Check the result type
                when (resultCode) {
                    //If successful, start tracking
                    RESULT_SUCCESS -> if (containsKey(RESULT_TRACKING_ID)) {
                        val share = Intent(this@MainActivity, ShareActivity::class.java)
                        val trackingId = getString(RESULT_TRACKING_ID)
                        share.putExtra(TRACKING_ID, trackingId)
                        startActivity(share)
                    }
                    //Otherwise log the error TODO give user a heads up
                    RESULT_ERROR -> printerr(getString(RESULT_ERROR_MESSAGE))
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Setup the toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        //Attach action to the main button
        startTracking.setOnClickListener {
            //Show the progress indicator
            startTracking.isEnabled = false
            trackingStartProgress.animate().alpha(1f)

            //Start service to retrieve a new tracking ID
            val startTracking = Intent(this, StartTrackingService::class.java)
            startTracking.putExtra(CALLBACK, startTrackingCallback)
            startService(startTracking)
        }
    }
}