package com.hackjunction.tulips.main

import android.app.IntentService
import android.content.Intent
import android.os.Bundle
import android.os.ResultReceiver
import com.hackjunction.tulips.ServerInfo
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import org.json.JSONObject
import java.io.IOException

class StartTrackingService : IntentService("StartTrackingService") {
    companion object {
        val CALLBACK = "StartTrackingCallback"

        val RESULT_SUCCESS = 0
        val RESULT_TRACKING_ID = "TrackingId"
        val RESULT_ERROR = 1
        val RESULT_ERROR_MESSAGE = "ErrorMessage"

        private val trackingIdKey = "trackerId"
    }

    override fun onHandleIntent(intent: Intent?) {
        //Ensure we have a way to communicate back to our activity
        val callback = if (intent?.hasExtra(CALLBACK) == true) {
            intent.getParcelableExtra<ResultReceiver>(CALLBACK)
        } else return

        //Setup the request for a new tracking ID
        val client = OkHttpClient()
        val emptyBody = RequestBody.create(null, ByteArray(0))
        val request = Request.Builder()
                .url(ServerInfo.NEW_TRACKER_ENDPOINT)
                .header("Content-Length", "0")
                .method("POST", emptyBody)
                .build()
        val result = Bundle()

        try {
            //Request a new ID
            val response = client.newCall(request).execute()

            //Make sure it was successfully added to the backend
            if (response.code() != 201) {
                result.putString(RESULT_ERROR_MESSAGE, "Wrong response code")
                callback.send(RESULT_ERROR, result)
                return
            }

            //Make sure we actually receive a tracking ID
            val jsonResponse = JSONObject(response.body()?.string())
            if (!jsonResponse.has(trackingIdKey)) {
                result.putString(RESULT_ERROR_MESSAGE, "Missing trackerId")
                callback.send(RESULT_ERROR, result)
                return
            }

            //Make sure it has the correct/expected lenght
            val trackingId = jsonResponse.getString(trackingIdKey)
            if (trackingId.length != 6) {
                result.putString(RESULT_ERROR_MESSAGE, "Wrong trackerId length")
                callback.send(RESULT_ERROR, result)
                return
            }

            //Hand the tracking ID back to the activity
            result.putString(RESULT_TRACKING_ID, trackingId)
            callback.send(RESULT_SUCCESS, result)
        } catch (e: IOException) {
            //Otherwise hand it back the error message
            result.putString(RESULT_ERROR_MESSAGE, e.message)
            callback.send(RESULT_ERROR, result)
        }
    }
}