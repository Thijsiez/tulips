package com.hackjunction.tulips.main

import android.app.IntentService
import android.content.Intent
import com.hackjunction.tulips.ServerInfo
import com.hackjunction.tulips.printerr
import okhttp3.FormBody
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.IOException

class GeofenceEnteredService : IntentService("GeofenceEnteredService") {
    companion object {
        val TRACKING_ID = "TrackingId"
        val ZONE_NAME = "ZoneName"

        private val trackingIdKey = "id"
        private val zoneNameKey = "zoneName"
    }

    override fun onHandleIntent(intent: Intent?) {
        if (intent == null) return

        //This code runs headless so there are no checks and no feedback
        val client = OkHttpClient()
        val body = FormBody.Builder()
                .add(trackingIdKey, intent.getStringExtra(TRACKING_ID))
                .add(zoneNameKey, intent.getStringExtra(ZONE_NAME))
                .build()
        val request = Request.Builder()
                .url(ServerInfo.GEOFENCE_ENTERED_ENDPOINT)
                .method("POST", body)
                .build()

        try {
            client.newCall(request).execute()
        } catch (e: IOException) {
            printerr(e.message)
        }
    }
}