package com.hackjunction.tulips.main

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.VibrationEffect.DEFAULT_AMPLITUDE
import android.os.Vibrator
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.hackjunction.tulips.ServerInfo
import io.proximi.proximiiolibrary.ProximiioAPI
import io.proximi.proximiiolibrary.ProximiioGeofence
import io.proximi.proximiiolibrary.ProximiioGoogleMapHelper
import io.proximi.proximiiolibrary.ProximiioListener
import kotlinx.android.synthetic.main.activity_share.*

class ShareActivity : AppCompatActivity(), OnMapReadyCallback {
    companion object {
        val TRACKING_ID = "TrackingId"

        private val copyVibrationDuration = 100L

        private val proximiIoApiTag = "Embrace"
        private val proximiIoApiAuthKey = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0eXAiOiJKV1Q" +
                "iLCJhbGciOiJIUzI1NiIsImlzcyI6ImUzNzFlOWRlLTIwY2MtNDUzMC1hNGI0LWVkNGY0NzdiNjIxN" +
                "CIsInR5cGUiOiJhcHBsaWNhdGlvbiIsImFwcGxpY2F0aW9uX2lkIjoiY2ZkNmUwMzYtNmQ2Zi00N2R" +
                "jLWI4ZmYtMjk2MjQzOTZmY2QyIn0.e225iitGznVFjginLknzupQtOpRu6XJTM4hYm7llMDk"
    }

    private lateinit var trackingId: String
    private lateinit var trackingUrlString: String

    private lateinit var proximiIoApi: ProximiioAPI
    private var proximiIoMapHelper: ProximiioGoogleMapHelper? = null


    private val proximiIoListener = object : ProximiioListener() {
        override fun geofenceEnter(geofence: ProximiioGeofence?) {
            val notifyBackend = Intent(this@ShareActivity, GeofenceEnteredService::class.java)
            notifyBackend.putExtra(GeofenceEnteredService.TRACKING_ID, trackingId)
            notifyBackend.putExtra(GeofenceEnteredService.ZONE_NAME, geofence?.name ?: return)
            startService(notifyBackend)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_share)

        //Setup the toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        //Read the tracking ID we received from our backend and main activity
        trackingId = intent.getStringExtra(TRACKING_ID)
        trackingUrlString = ServerInfo.getTrackingUrlString(trackingId)
        trackingUrl.text = trackingUrlString

        //Setup the ability to easily copy the tracking URL to the clipboard
        val clipboardManager = getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
        val vibratorManager = getSystemService(VIBRATOR_SERVICE) as Vibrator
        trackingUrl.setOnLongClickListener {
            //Actually copy the tracking URL
            clipboardManager.primaryClip = ClipData
                    .newPlainText(getString(R.string.liveTrackingUrl), trackingUrlString)

            //Give haptic feedback
            if (vibratorManager.hasVibrator()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    vibratorManager.vibrate(VibrationEffect
                            .createOneShot(copyVibrationDuration, DEFAULT_AMPLITUDE))
                } else {
                    @Suppress("DEPRECATION")
                    vibratorManager.vibrate(copyVibrationDuration)
                }
            }

            //Give visual feedback
            Toast.makeText(this, R.string.urlCopiedToClipboard, Toast.LENGTH_LONG).show()

            return@setOnLongClickListener true
        }

        //Setup the ability to directly share the tracking URL
        val shareUrlAction = Intent(Intent.ACTION_SEND)
        shareUrlAction.putExtra(Intent.EXTRA_TEXT, trackingUrlString)
        shareUrlAction.type = "text/plain"
        val shareIntent = Intent.createChooser(shareUrlAction,
                getString(R.string.shareYourLiveLocation))
        shareTrackingUrl.setOnClickListener {
            startActivity(shareIntent)
        }

        //Setup the ProximiIO API
        proximiIoApi = ProximiioAPI(proximiIoApiTag, this)
        proximiIoApi.setListener(proximiIoListener)
        proximiIoApi.setAuth(proximiIoApiAuthKey)
        proximiIoApi.setActivity(this)

        //Setup the Google Map fragment
        val mapFragment = supportFragmentManager.findFragmentById(R.id.mapView)
        (mapFragment as SupportMapFragment).getMapAsync(this)
    }

    override fun onDestroy() {
        super.onDestroy()

        //Release what is no longer needed
        proximiIoMapHelper?.destroy()
        proximiIoApi.destroy()
    }

    override fun onMapReady(map: GoogleMap?) {
        proximiIoMapHelper = ProximiioGoogleMapHelper.Builder(this, map ?: return)
                .autoRotateAndFollow(true)
                .floorID("54e97082-c362-45dd-a674-e38c81b77c07")
                .showFloorPlan(true)
                .build()

        //Connect the ProximiIO helper and Google Map
        map.setOnCameraIdleListener(proximiIoMapHelper)
        map.setOnCameraMoveStartedListener(proximiIoMapHelper)
        map.setOnMapClickListener(proximiIoMapHelper)
        map.setOnMarkerClickListener(proximiIoMapHelper)
        map.setOnMyLocationButtonClickListener(proximiIoMapHelper)
    }
}